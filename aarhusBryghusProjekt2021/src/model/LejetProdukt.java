package model;

public class LejetProdukt extends Produkt {

	private boolean erUdlejet;
	
	public LejetProdukt(String navn, String beskrivelse, boolean erUdlejet) 
	{
	   super(navn, beskrivelse);
       this.erUdlejet = erUdlejet;
	}	
	public boolean isErUdlejet() {
		return erUdlejet;
	}
	//Checker om produktet er udlejet.
	public void setErUdlejet(boolean erUdlejet) {
		this.erUdlejet = erUdlejet;
	}

	@Override
	public String toString() {
		return super.getNavn();
	}

	
}
