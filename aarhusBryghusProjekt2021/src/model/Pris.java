package model;

public class Pris {
	private double beløb;
	private double pant;
	private int klippekort;
	private Produkt produkt;
	private Prisliste prisListe;
	
	public Pris(double beløb, double pant, int klippekort, Prisliste prisListe, Produkt produkt) {
		this.beløb = beløb;
		this.pant = pant;
		this.setKlippekort(klippekort);
		this.produkt = produkt;
		this.setPrisListe(prisListe);
		
	}

	public double getBeløb() {
		return beløb;
	}

	public void setBeløb(double beløb) {
		this.beløb = beløb;
	}

	public double getPant() {
		return pant;
	}

	public void setPant(double pant) {
		this.pant = pant;
	}

	public int getKlippekort() {
		return klippekort;
	}

	public void setKlippekort(int klippekort) {
		this.klippekort = klippekort;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		if (this.produkt != produkt) {
			Produkt oldProd = this.produkt;
			if (oldProd != null) {
				oldProd.removePris(this);
			}
			this.produkt = produkt;
			if (produkt != null) {
				produkt.addPris(this);
			}
		}
	}	
	public Prisliste getPrisListe() {
		return prisListe;
	}

	public void setPrisListe(Prisliste prisListe) {
		if (this.prisListe != prisListe) {
			Prisliste oldPrisl = this.prisListe;
			if (oldPrisl != null) {
				oldPrisl.removePris(this);
			}
			this.prisListe = prisListe;
			if (prisListe != null) {
				prisListe.addPris(this);
			}
		}
	}
	
	@Override
	public String toString() {
		return "" + produkt + "\n " + beløb + "\n" + "( " + pant + " i pant ) " + "\n" + "( " + klippekort + " i klip";
	}
}


