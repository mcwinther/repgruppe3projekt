package model;

public class Ordrelinje {
	
	private int antal;
	private Pris pris;
	
	public Ordrelinje(int antal) 
	{
		this.antal = antal;
	}
	
	public int getAntal() {
		return antal;
	}

	public void setAntal(int antal) {
		this.antal = antal;
	}

	public Pris getPris() {
		return pris;
	}

	public void setPris(Pris pris) {
		if(this.pris != pris) {
			this.pris = pris;
		}
	}

	@Override
	public String toString() {
		return pris.getProdukt().getNavn() + " " + antal + "x" + "\n" + pris.getBeløb() + " kr pr. antal" +"\n" + pris.getPant() + " (kr. i pant)";
	}

	
}
