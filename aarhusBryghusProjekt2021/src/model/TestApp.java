package model;

import java.time.LocalDate;

public class TestApp {

	public static void main(String[] args) {
		Produkt p1 = new Produkt("Trøje", "Aarhus Bryghus trøje");
		Prisliste pl1 = new Prisliste("Type 1");
		Pris pr1 = new Pris(200.00, 0, 0, pl1, p1);
		//-----------------------------------------------------------------
		Ordre o1 = new Ordre(1, "Ordre", "Ordre 1 er modtaget", "Mastercard", LocalDate.of(2021, 2, 1));
		Ordre o2 = new Ordre(2, "Ordre", "Ordre 1 er modtaget", "Mastercard", LocalDate.of(2021, 2, 3));
		Ordrelinje ol1 = new Ordrelinje(2);
		//-----------------------------------------------------------------
		Event e1 = new Event("SommerFest", "Laves i mosegård", LocalDate.of(2021, 2, 10), "Said");
		LejetProdukt lp1 = new LejetProdukt("Fadøl", "en ØL", true);
		//-----------------------------------------------------------------
		p1.addPris(pr1);
		System.out.println(p1.getPriser());		
		System.out.println(pr1.getBeløb());		
		o1.addOrdrelinje(ol1);
		o2.addOrdrelinje(ol1);
		System.out.println(ol1.getPris());		
		e1.addPris(pr1);
		lp1.addPris(pr1);
		System.out.println(e1.getPriser());
	}

}
