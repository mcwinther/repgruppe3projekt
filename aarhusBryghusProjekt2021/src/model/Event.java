package model;

import java.time.LocalDate;

public class Event extends Produkt {
	private LocalDate dato;
	private String arrangør;

	public Event(String navn, String beskrivelse, LocalDate dato, String arrangør) {
		super(navn, beskrivelse);
		this.dato = dato;
		this.arrangør = arrangør;
	}

	public LocalDate getDato() {
		return dato;
	}

	public void setDato(LocalDate dato) {
		this.dato = dato;
	}

	public String getArrangør() {
		return arrangør;
	}

	public void setArrangør(String arrangør) {
		this.arrangør = arrangør;
	}

	@Override
	public String toString() {
		return super.getNavn() + " " + super.getBeskrivelse() + " " + arrangør + " " + dato;
	}

}
