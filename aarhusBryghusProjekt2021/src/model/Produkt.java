package model;

import java.util.ArrayList;

public class Produkt {
	private String navn;
	private String beskrivelse;
	private ArrayList<Pris> priser;
	
	public Produkt(String navn, String beskrivelse) {
		this.navn = navn;
		this.beskrivelse = beskrivelse;
		priser = new ArrayList<>();
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public ArrayList<Pris> getPriser() {
		return new ArrayList<Pris>(priser);
	}

	public void addPris(Pris pris) {
		if(!priser.contains(pris)){
			priser.add(pris);
			pris.setProdukt(this);
		}
	}
	
	public void removePris(Pris pris) {
		if(priser.contains(pris)){
			priser.remove(pris);
			pris.setProdukt(this);
		}
	}

	@Override
	public String toString() {
		return navn + "\n" + beskrivelse;
	}
	
	
	
	
	

}
