package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Ordre {
	private int ordrerNummer;
	private String navn;
	private String beskrivelse;
	private String betalingsform;
	private LocalDate salgsDato;
	private final ArrayList<Ordrelinje> ordrelinjer;

	public Ordre(int ordrerNummer, String navn, String beskrivelse, String betalingsform,
			LocalDate salgsDato) {
		this.ordrerNummer = ordrerNummer;
		this.navn = navn;
		this.beskrivelse = beskrivelse;
		this.betalingsform = betalingsform;
		this.salgsDato = salgsDato;
		ordrelinjer = new ArrayList<>();
	}

	public int getOrdrerNummer() {
		return ordrerNummer;
	}

	public void setOrdrerNummer(int ordrerNummer) {
		this.ordrerNummer = ordrerNummer;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public String getBetalingsform() {
		return betalingsform;
	}

	public void setBetalingsform(String betalingsform) {
		this.betalingsform = betalingsform;
	}


	public LocalDate getSalgsDato() {
		return salgsDato;
	}

	public void setSalgsDato(LocalDate salgsDato) {
		this.salgsDato = salgsDato;
	}

	public ArrayList<Ordrelinje> getOrdrelinjer() {
		return new ArrayList<>(ordrelinjer);
	}

	public void addOrdrelinje(Ordrelinje o) {
		if (!ordrelinjer.contains(o)) {
			ordrelinjer.add(o);
		}
	}

	public void removeOrdrelinje(Ordrelinje o) {
		if (ordrelinjer.contains(o)) {
			ordrelinjer.remove(o);
		}
	}
/*Metoden tager alle ordrelinjer og ser på priserne og den salgssituation, de befinder sig i
 * Hvis der er klippekort på prisen, bliver prisen sat til klippekortene, som en kunde betaler med.
 * Hvis et tilhørende produkt på en pris er et lejetProdukt bliver beløbet sat til at være lig med pant.
 */
	public double beregnSamletPris() {
		double sum = 0;
		for (Ordrelinje ordrelinje : ordrelinjer) {
		if(ordrelinje.getPris().getPrisListe().getPrisListeType().equals("Fredagsbar")){
			if(ordrelinje.getPris().getKlippekort() > 0) 
			{
				if(betalingsform.equals("Klippekort")) 
				{
					  sum += ordrelinje.getPris().getKlippekort() * ordrelinje.getAntal();
				}
				else 
				{
					sum = ordrelinje.getPris().getKlippekort() * ordrelinje.getAntal();
				}
				
			}
			else 
			{
				sum += (ordrelinje.getPris().getBeløb() *ordrelinje.getAntal());
			}
			
		}
		else 
		{
			if(!(ordrelinje.getPris().getProdukt() instanceof LejetProdukt)) {
				sum += (ordrelinje.getPris().getBeløb() *ordrelinje.getAntal());	
				
			}
			else {
				sum += ordrelinje.getPris().getPant();
			}
		}
		
						
	}
		return sum;
	}
	/*
	 * Denne metoder finder antaællet af priser i alle ordrelinjer.
	 */
	public int getSamletAntal() {
		int sum = 0;
		for (Ordrelinje ol: ordrelinjer) 
		{
			sum += ol.getAntal();
		}
		return sum;
	}
	
	@Override
	public String toString() {
		return "Ordre " + ordrerNummer;
	}

}
