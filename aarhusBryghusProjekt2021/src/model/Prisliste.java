package model;

import java.util.ArrayList;

public class Prisliste {

	private ArrayList<Pris> priser; 
	private String prisListeType;
	
	public Prisliste(String prisListeType) {
		priser = new ArrayList<Pris>();
		this.prisListeType = prisListeType;
	}

	public ArrayList<Pris> getPris() {
		return new ArrayList<Pris>(priser);
	}

	public void addPris(Pris pris) {
		if(!priser.contains(pris)) {
			priser.add(pris);
			pris.setPrisListe(this);
		}
	}

	public void removePris(Pris pris) {
		if(priser.contains(pris)) {
			priser.remove(pris);
			pris.setPrisListe(this);
		}
	}
	
	public Pris createPris(double beløb, double pant, int klippekort, Prisliste prisListe, Produkt produkt) {
		if(beløb < 0) {
			throw new ArithmeticException("Beløb er mindre end 0");
		}
		else {
			Pris pris = new Pris(beløb, pant, klippekort, prisListe, produkt);
			priser.add(pris);
			return pris;
		}
	}
	
	public String getPrisListeType() {
		return prisListeType;
	}

	public void setPrisListeType(String prisListeType) {
		this.prisListeType = prisListeType;
	}

	@Override
	public String toString() {
		return "Prisliste " + prisListeType;
	}

}
