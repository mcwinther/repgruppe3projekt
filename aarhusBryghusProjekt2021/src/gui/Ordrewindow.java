package gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.LejetProdukt;
import model.Ordre;
import model.Ordrelinje;
import model.Pris;
import model.Prisliste;
import model.Produkt;

public class Ordrewindow extends Stage {

	private Controller controller;

	public Ordrewindow(String tittle) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setTitle(tittle);
		GridPane pane = new GridPane();
		// Initialiserer controller og data i initStorage
		controller = Controller.getController();
		initContent(pane);
		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private double samletPris = 0;

	private final ComboBox<Pris> cbbpriser = new ComboBox<>();
	private final ComboBox<Prisliste> cbbPrislister = new ComboBox<>();
	private final ComboBox<String> cbbbetalingsmetoder = new ComboBox<>();
	private final ArrayList<String> betalingsmetoder = new ArrayList<>();
	private final ListView<Ordrelinje> lvwOrdrelinjer = new ListView<>();

	private DatePicker dpDato = new DatePicker();

	// Textfields
	private TextField txtAntal = new TextField();
	private TextField txtNavn = new TextField();
	private TextField txtProduktNavn = new TextField();
	private TextField txfPrisliste = new TextField();
	private TextField txfSamletPris = new TextField();
	// Labels
	private Label lblAntalProdukkt = new Label("Antal");
	private Label lblVælgprisliste = new Label("Vælg prisliste:");
	private Label lblVælgProdukt = new Label("Vælg produkt");
	private Label lblProduktnavn = new Label("Produktnavn");
	private Label lblPris = new Label("Samlet pris:");
	private Label lblPrisliste = new Label("Prisliste");
	// Disser labels og textArea er for kundeoplysninger
	private TextArea beskrivelse = new TextArea();
	private Label lblBeskrivelse = new Label("Beskrivelse");
	private Label lblKundenavn = new Label("Kunde");
	private Label lblBetalingsmetode = new Label("Betalingsmetode");
	private Label lblSalgsDato = new Label("Salgdato");

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(5);
		pane.setVgap(5);
		pane.setPrefHeight(600);
		pane.setPrefWidth(1000);
		Button btnTilføj = new Button("Godkend ordre");
		pane.add(btnTilføj, 12, 6);
		btnTilføj.setOnAction(event -> this.okAction());
		pane.add(lblAntalProdukkt, 4, 4);
		Button btnPlus = new Button("+");
		pane.add(btnPlus, 6, 4);
		btnPlus.setOnAction(event -> this.incrementAction());
		Button btnMinus = new Button("-");
		pane.add(btnMinus, 7, 4);
		btnMinus.setOnAction(event -> this.decrementAction());
		pane.add(lblProduktnavn, 4, 2);
		pane.add(txtProduktNavn, 5, 2);
		txtProduktNavn.setEditable(false);
		pane.add(lblPrisliste, 4, 3);
		pane.add(txfPrisliste, 5, 3);
		txfPrisliste.setEditable(false);
		pane.add(txtAntal, 5, 4);
		pane.add(lvwOrdrelinjer, 2, 11);
		lvwOrdrelinjer.setPrefSize(200, 200);
		Label lblProdukter = new Label("Produkter i ordrelinje:");
		pane.add(lblProdukter, 2, 10);
		pane.add(lblPris, 2, 14);
		pane.add(txfSamletPris, 2, 15);
		Button btnTilføjOrdrelinje = new Button("Tilføj produkt \n til ordrelinje");
		pane.add(btnTilføjOrdrelinje, 5, 5);
		btnTilføjOrdrelinje.setOnAction(event -> this.addOrdrelinje());
		Button btnFjern = new Button("Fjern produkt fra ordrelinje");
		btnFjern.setOnAction(event -> this.removeOrdrelinje());
		pane.add(btnFjern, 2, 12);
		// Tilføejer indhold i Comboboxes
		pane.add(cbbpriser, 2, 4);
		pane.add(cbbPrislister, 2, 2);
		betalingsmetoder.add("MobilePay");
		betalingsmetoder.add("Kontant");
		betalingsmetoder.add("Mastercard");
		betalingsmetoder.add("Klippekort");
		betalingsmetoder.add("Debit kort");
		cbbbetalingsmetoder.getItems().setAll(betalingsmetoder);
		cbbPrislister.getItems().setAll(controller.getPrislister());
		// Tilføjer Labels
		pane.add(lblVælgprisliste, 2, 1);
		pane.add(lblVælgProdukt, 2, 3);
		pane.add(lblKundenavn, 12, 2);
		pane.add(lblBeskrivelse, 12, 3);
		pane.add(lblBetalingsmetode, 12, 4);
		pane.add(lblSalgsDato, 12, 5);

		// Tilføjer TextFields
		pane.add(cbbbetalingsmetoder, 13, 4);
		pane.add(txtNavn, 13, 2);
		pane.add(beskrivelse, 13, 3);
		beskrivelse.setPrefSize(100, 50);
		pane.add(dpDato, 13, 5);

		// Ordner comboboxes
		ChangeListener<Prisliste> prisListeListener = (ov, oldPrisListe, newPrisListe) -> this
				.selectionChangedPrisListe();
		cbbPrislister.getSelectionModel().selectedItemProperty().addListener(prisListeListener);

		ChangeListener<String> betalingsListenet = (ov, oldBetalingsform, newBetalingsform) -> this
				.selectionChangedProdukter();
		cbbbetalingsmetoder.getSelectionModel().selectedItemProperty().addListener(betalingsListenet);
	}

	private void selectionChangedProdukter() {
		Pris pris = cbbpriser.getSelectionModel().getSelectedItem();
		if (pris != null) {
			updatePris();
			txtAntal.setText("" + 1);
			txfPrisliste.setText(pris.getPrisListe().getPrisListeType());
			txtProduktNavn.setText(pris.getProdukt().getNavn());
		}

	}

	private void selectionChangedPrisListe() {
		Prisliste prisliste = cbbPrislister.getSelectionModel().getSelectedItem();
		if (prisliste != null) {
			cbbpriser.getItems().setAll(prisliste.getPris());
		}
	}

	private void incrementAction() {
		Pris pris = cbbpriser.getSelectionModel().getSelectedItem();
		if (pris != null) {
			String antal = txtAntal.getText();
			int intAntal = Integer.parseInt(antal);
			intAntal++;
			txtAntal.setText("" + intAntal);
		}

	}

	private void decrementAction() {

		Pris pris = cbbpriser.getSelectionModel().getSelectedItem();
		if (pris != null) {
			String antal = txtAntal.getText();
			int intAntal = Integer.parseInt(antal);
			intAntal--;
			txtAntal.setText("" + intAntal);
		}
	}

	private void addOrdrelinje() {
		String betalingsmetode = cbbbetalingsmetoder.getSelectionModel().getSelectedItem();
		String antal = txtAntal.getText();
		int intAntal = Integer.parseInt(antal);
		Pris pris = cbbpriser.getSelectionModel().getSelectedItem();
		Ordrelinje ordrelinje = new Ordrelinje(intAntal);

		if (betalingsmetode.equals("Klippekort")) {
			updatePris();
		} else {
			updatePris();
		}

		ordrelinje.setPris(pris);

		lvwOrdrelinjer.getItems().add(ordrelinje);
		txtProduktNavn.clear();
		txfPrisliste.clear();
		updatePris();

	}

	private void okAction() {
		String name = txtNavn.getText().trim();
		String ordrebeskrivelse = beskrivelse.getText().trim();
		String betalingsform = cbbbetalingsmetoder.getSelectionModel().getSelectedItem();
		String antal = txtAntal.getText();
		int intAntal = Integer.parseInt(antal);

		if (name.length() > 0 && ordrebeskrivelse.length() > 0 && betalingsform != null && intAntal > 0 && dpDato.getValue() != null) {
			if (cbbbetalingsmetoder.getSelectionModel().getSelectedItem().equals("Klippekort")
					&& cbbpriser.getSelectionModel().getSelectedItem().getKlippekort() == 0) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Fejl");
				alert.setHeaderText("Handling forbudt");
				alert.setContentText("Du kan ikke vælge klippekort, når et produkt ikke kan købes med klippekort");
				alert.show();
			} else {

				Random rand = new Random();
				int ordreNummer = rand.nextInt(10_000);
				LocalDate dato = dpDato.getValue();
				Ordre o = controller.createOrdre(ordreNummer, name, ordrebeskrivelse, betalingsform, dato);
				for (Ordrelinje ordrelinje : lvwOrdrelinjer.getItems()) {
					o.addOrdrelinje(ordrelinje);
				}
				for (Ordrelinje ol : o.getOrdrelinjer()) 
				{
					if(ol.getPris().getProdukt() instanceof LejetProdukt) 
					{
						LejetProdukt produkt = (LejetProdukt) ol.getPris().getProdukt();
						controller.udlejProdukt(produkt);
					}
				}			
				hide();

			}

		} else {

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Fejl");
			alert.setHeaderText("Information mangler");
			alert.setContentText("Indtast navn, beskrivelse, betalingsform, dato og antal er mindre end 0");
			alert.show();
		}

	}

	private void updatePris() {
		// Nulstiller prisen hver gang jeg opdaterer den samlede ordre
		samletPris = 0;
		String betalingsmetode = cbbbetalingsmetoder.getSelectionModel().getSelectedItem();
		for (Ordrelinje ol : lvwOrdrelinjer.getItems()) {
            if (ol.getPris().getPrisListe().getPrisListeType().equals("Fredagsbar")) {
                if (ol.getPris().getKlippekort() > 0) {
                    if (betalingsmetode.equals("Klippekort")) {
                        samletPris += ol.getPris().getKlippekort() * ol.getAntal();
                    }

                    else {
                        samletPris += ol.getPris().getBeløb() * ol.getAntal();
                    }
                } 
                else {
                    samletPris += ol.getPris().getBeløb() * ol.getAntal();
                }

            } else {
                if (!(ol.getPris().getProdukt() instanceof LejetProdukt)) {
                    samletPris += ol.getPris().getBeløb() * ol.getAntal();
                } else {
                    samletPris += ol.getPris().getPant() * ol.getAntal();
                }
            }

        }
        txfSamletPris.setText("" + samletPris);

    }
	private void removeOrdrelinje() {
		Ordrelinje ordrelinje = lvwOrdrelinjer.getSelectionModel().getSelectedItem();
		if (ordrelinje != null) {
			lvwOrdrelinjer.getItems().remove(ordrelinje);
			updatePris();
		}
	}
}
