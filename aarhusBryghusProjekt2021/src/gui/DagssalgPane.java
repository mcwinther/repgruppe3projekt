package gui;

import java.time.LocalDate;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Ordre;
import model.Ordrelinje;
import model.Pris;
import model.Produkt;

public class DagssalgPane extends GridPane
{
	private final ListView<Ordre> lvwOrdre = new ListView<>();
	private final ComboBox<LocalDate> cbbDatoer = new ComboBox<>();
    private final ListView<Ordrelinje> lvwOrdrerPrProdukt = new ListView<>();
	
	private Controller controller;
	private Label lblOrdrer = new Label("Ordrer:");
	private Label lblProduktPrOrdre = new Label("Produkter i Ordre");
	private Label lblBetalingsform = new Label("Betalingsform");
	private Label lblSamletPris = new Label("Samlet pris");
	private Label lblDato = new Label("Dato");
	private Label lblSamletAntal = new Label("Samlet antal produkter\n i ordre");
	
	//TextFields
	private final TextField txfSamletAntalProdukt = new TextField();
	private final TextField txfSamletPris = new TextField();
	private final TextField txfBetalingsform = new TextField();
	
	public DagssalgPane() 
	{
		this.setPadding(new Insets(20));
		this.setHgap(10);
		this.setVgap(10);
		lvwOrdre.setPrefHeight(100);
		lvwOrdre.setPrefWidth(200);
		this.add(lblDato, 0, 2);
		this.add(cbbDatoer, 1, 2);
		this.add(lvwOrdre, 1, 4);
		lvwOrdre.getItems().clear();
		this.add(lblProduktPrOrdre, 2, 3);
		this.add(lvwOrdrerPrProdukt, 2, 4);
		lvwOrdrerPrProdukt.setPrefSize(200, 100);
		//Tilføjer Labels		
		this.add(lblOrdrer, 1, 3);
	    this.add(lblBetalingsform, 1, 6);
	    this.add(lblSamletAntal, 1, 5);
	    this.add(lblSamletPris, 1, 7);
		//Tilføjer TextFields
		this.add(txfSamletAntalProdukt, 2, 5);
		this.add(txfBetalingsform, 2, 6);
		this.add(txfSamletPris, 2, 7);		
		txfSamletAntalProdukt.setEditable(false);
		txfBetalingsform.setEditable(false);
		txfSamletPris.setEditable(false);
		//Tilføjer controlleren
		controller = Controller.getController();						
		for (Ordre o : controller.getOrdrer()) 
		{
			cbbDatoer.getItems().add(o.getSalgsDato());
		}				
		//Disse fungerer som triggere, der viser relevant data afhængig af det valgte
		ChangeListener<LocalDate> datoListener = (ov, oldDato, newDato) -> this.cbbSelectionChanged();
		cbbDatoer.getSelectionModel().selectedItemProperty().addListener(datoListener);
		ChangeListener<Ordre> ordreListener = (ov, oldDato, newdDato) -> this.ordreSelectionChanged();
		lvwOrdre.getSelectionModel().selectedItemProperty().addListener(ordreListener);
	}
	
	private void cbbSelectionChanged() 
	{
		if(lvwOrdre.hasProperties()) 
		{
			lvwOrdre.getItems().clear();
		}
 		LocalDate salgsDato = cbbDatoer.getSelectionModel().getSelectedItem();
 		if(salgsDato != null) 
 		{
 			for (Ordre o : controller.getOrdrer()) {
				if(salgsDato.equals(o.getSalgsDato())) 
				{
					lvwOrdre.getItems().add(o);
				}
			}
 		}

	}
	
	private void ordreSelectionChanged() 
	{
		if(lvwOrdrerPrProdukt.hasProperties()) 
		{
			lvwOrdrerPrProdukt.getItems().clear();
		}
		Ordre ordre = lvwOrdre.getSelectionModel().getSelectedItem();		
		if(ordre != null) 
		{
			txfSamletAntalProdukt.setText(""+ordre.getSamletAntal());
			txfBetalingsform.setText(ordre.getBetalingsform());
			txfSamletPris.setText(""+ordre.beregnSamletPris());
			lvwOrdrerPrProdukt.getItems().setAll(ordre.getOrdrelinjer());
		}
	}
	//Metoden nulstiller information og opretter det igen, hver gang man åbner fanen påny
	public void updateControls() 
	{
		txfSamletAntalProdukt.clear();
		txfBetalingsform.clear();
        txfSamletPris.clear();
		if(lvwOrdre.hasProperties()) 
		{
			lvwOrdre.getItems().clear();
		}
		
		if(cbbDatoer.hasProperties()) 
		{
			cbbDatoer.getItems().clear();
		}
		
		controller = Controller.getController();						
		for (Ordre o : controller.getOrdrer()) {
			if(!cbbDatoer.getItems().contains(o.getSalgsDato())){
				cbbDatoer.getItems().add(o.getSalgsDato());
			}
			
		}	
		
	}
	
}
	
	



