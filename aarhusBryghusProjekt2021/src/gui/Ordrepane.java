package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Ordre;
import model.Ordrelinje;

public class Ordrepane extends GridPane{
	private final ListView<Ordre> lvwOrdre = new ListView<>();
	private final ListView<Ordrelinje> lvwOrdrelinjer = new ListView<>();
	private Button tilføjProdukt;
	private Controller controller;
	private double samletPris;
	private TextField txfpris = new TextField();
	private TextField txfNavn = new TextField();
	private TextArea txfBeskrivelse = new TextArea();
	private TextField txfBetalingsform = new TextField();
    private TextField txfDato = new TextField();
    private Label lblBeskrivelse = new Label("Beskrivelse");
    
	public Ordrepane() 
	{
		controller = Controller.getController();
		this.setPadding(new Insets(20));
		this.setHgap(10);
		this.setVgap(10);
		Label lblBetalingsmetode = new Label("Betalingsmetode");
		Label lblProdukterIOrdre = new Label("Produkter i ordre:");
		Label lblOrdre = new Label("Ordre");
		this.add(lblOrdre, 1, 1);
		this.add(lvwOrdre, 1, 2);
		tilføjProdukt = new Button("Tilføj Ordre");
		this.add(tilføjProdukt, 1, 4);
		tilføjProdukt.setOnAction(event->this.createAction());
		this.add(lblProdukterIOrdre, 2, 1);
		this.add(lvwOrdrelinjer, 2, 2);
		lvwOrdre.getItems().setAll(controller.getOrdrer());
		this.add(txfDato, 2, 7);
		Label lblDato = new Label("Dato");
		this.add(lblDato, 1, 7);
		Label lblPris = new Label("Pris");
		this.add(lblPris, 1, 9);
		Label lblKundenavn = new Label("Kunde");
		this.add(lblBetalingsmetode, 1, 8);
		this.add(lblKundenavn, 1, 5);
		this.add(lblBeskrivelse, 1, 6);
		this.add(txfNavn, 2, 5);		
		this.add(txfBeskrivelse, 2, 6);
		txfBeskrivelse.setPrefSize(50, 50);
		this.add(txfBetalingsform, 2, 8);
		txfNavn.setEditable(false);
		txfBeskrivelse.setEditable(false);
		txfBetalingsform.setEditable(false);
		txfDato.setEditable(false);
		lvwOrdrelinjer.setEditable(false);
		txfpris.setText("0");
		this.add(txfpris, 2, 9);
		txfpris.setEditable(false);
		//Trigger, der viser data efter valgte element.
		ChangeListener<Ordre> ordreListener = (ov, oldOrdre, newOrdre) -> this.beregnAction();
		lvwOrdre.getSelectionModel().selectedItemProperty().addListener(ordreListener);	
	}
	
	private void createAction() 
	{
		Ordrewindow ow = new Ordrewindow("Ny ordre");
		ow.showAndWait();		
		lvwOrdre.getItems().setAll(controller.getOrdrer());
	}
	
	
	private void beregnAction() {
		samletPris = 0;
		Ordre o = lvwOrdre.getSelectionModel().getSelectedItem();
		lvwOrdrelinjer.getItems().setAll(o.getOrdrelinjer());
		samletPris = o.beregnSamletPris();
		txfpris.setText(" " + samletPris);
		txfNavn.setText(o.getNavn());
		txfBeskrivelse.setText(o.getBeskrivelse());
		txfBetalingsform.setText(o.getBetalingsform());
		txfDato.setText(o.getSalgsDato().toString());
	}
	
}

