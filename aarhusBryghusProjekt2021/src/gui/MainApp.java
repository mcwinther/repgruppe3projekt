package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import controller.Controller;

public class MainApp extends Application {

	private Controller controller;
	
	public void init() {
		//Opretter et Controller objekt og initialiserer det med default data, så vinduet i GUI ikke er tomt
		controller = Controller.getController();
		controller.initStorage();
	}
	
	@Override
	//Dette er startvinduet.
	public void start(Stage stage) throws Exception 
	{
		stage.setTitle("Kasseapperatet");
		BorderPane pane = new BorderPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();		
	}
	
	private void initContent(BorderPane pane) {
        TabPane tabPane = new TabPane();
        this.initTabPane(tabPane);
	    pane.setPrefHeight(500);
	    pane.setPrefWidth(1000);
	    pane.setCenter(tabPane);
	}
	
	private void initTabPane(TabPane tabPane) {	
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);		
		Tab tabOrdre = new Tab("Ordre");
		tabPane.getTabs().add(tabOrdre);
		Tab tabDagssalg = new Tab("Dagssalg");
		tabPane.getTabs().add(tabDagssalg);
		Tab tabOversigt = new Tab("UdlejedeProdukterOversigt");
		tabPane.getTabs().add(tabOversigt);
		Ordrepane oPane = new Ordrepane();
		tabOrdre.setContent(oPane);
		DagssalgPane dsp = new DagssalgPane();
		tabDagssalg.setContent(dsp);
		tabDagssalg.setOnSelectionChanged(event -> dsp.updateControls());
		OversigtPane op = new OversigtPane();
		tabOversigt.setContent(op);
		tabOversigt.setOnSelectionChanged(event -> op.updateControls());	
	}

}
