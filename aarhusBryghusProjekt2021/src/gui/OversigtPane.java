package gui;

import java.util.ArrayList;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.LejetProdukt;
import model.Ordre;
import model.Ordrelinje;
import model.Pris;
import model.Produkt;

public class OversigtPane extends GridPane {

	private final ListView<Pris> lvwProdukter = new ListView<>();

	private Controller controller;
	private Label lblUdlejer = new Label("Udlejer:");
	private Label lblProdukt = new Label("Udlejede produkter:");
	private Label lblUdlejningsDato = new Label("Udlejsdato");
	private Label lblBrugteVarer = new Label("Antal brugte varer");
	private Label lblTilbageleveredeFustager = new Label("Tilbageleverede fustager og kulsyrepatroner");
	private Label lblTilbageleveret = new Label("Tilbageleveret");
	private TextField txfUdlejer = new TextField();
	private TextField txfDatoForUdlejning = new TextField();
	private TextField txfBrugteVarer = new TextField();
	private TextField txfTilbageleveredeFustager = new TextField();
	private CheckBox cbAfleveret = new CheckBox();
	private Button btnAfregnPris = new Button("Afregn pris");
	private TextArea infoBox = new TextArea();

	public OversigtPane() {
		controller = Controller.getController();
		this.setPadding(new Insets(20));
		this.setHgap(10);
		this.setVgap(10);	
		this.add(lblUdlejer, 0, 1);
		this.add(txfUdlejer, 0, 2);
		txfUdlejer.setEditable(false);
		this.add(txfDatoForUdlejning, 2, 2);
		txfDatoForUdlejning.setEditable(false);
		this.add(lblUdlejningsDato, 2, 1);
		this.add(lblProdukt, 0, 3);
		this.add(lvwProdukter, 0, 4);
		this.add(txfBrugteVarer, 5, 1);
		this.add(lblBrugteVarer, 4, 1);
		txfBrugteVarer.setDisable(true);
		this.add(lblTilbageleveredeFustager, 4, 2);
		this.add(txfTilbageleveredeFustager, 5, 2);
		txfTilbageleveredeFustager.setDisable(true);
		this.add(lblTilbageleveret, 4, 0);
		this.add(cbAfleveret, 5, 0);
		cbAfleveret.setDisable(true);
		btnAfregnPris.setDisable(true);
		cbAfleveret.setOnAction(event -> this.selectionChangedUdfyld());
		lvwProdukter.setPrefWidth(200);
		lvwProdukter.setPrefHeight(350);
		//Trigger, der viser information afhængig af det valgte element.
		ChangeListener<Pris> listener = (ov, oldProdukt, newProdukt) -> this.selectionChanged();
		lvwProdukter.getSelectionModel().selectedItemProperty().addListener(listener);
		this.add(btnAfregnPris, 5, 3);
		this.add(infoBox, 5, 4);
		infoBox.setEditable(false);
		btnAfregnPris.setOnAction(event -> this.afregnPris());


	}

	private void selectionChanged() {
		cbAfleveret.setDisable(false);
		LejetProdukt lp = null;
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem().getProdukt();
		if(produkt instanceof LejetProdukt){	
			lp = (LejetProdukt) produkt;
			if (lp != null) {		
				if(lp.isErUdlejet()) {
					for (Ordre o : controller.getOrdrer()) {
						for (Ordrelinje ol : o.getOrdrelinjer()) {				
							Pris pris = ol.getPris();
							if(pris.getProdukt().equals(lp)) {							
								txfUdlejer.setText(o.getNavn());
								txfDatoForUdlejning.setText("" + o.getSalgsDato());
						}				
					}
		}
		
			}
			
		}

			
		}

			}
	
	private void selectionChangedUdfyld() {
	if(cbAfleveret.isSelected()) 
	{
		btnAfregnPris.setDisable(false);
		txfBrugteVarer.setDisable(false);
		txfTilbageleveredeFustager.setDisable(false);		
	}
	else 
	{
		btnAfregnPris.setDisable(true);
		txfBrugteVarer.setDisable(true);
		txfTilbageleveredeFustager.setDisable(true);
		cbAfleveret.setDisable(false);
		txfBrugteVarer.clear();
		txfTilbageleveredeFustager.clear();
	}
	
}
	//Metode til at nulstille og oprette data, hver gang man åbner fanen.
	public void updateControls() {
			txfUdlejer.clear();
			txfDatoForUdlejning.clear();
			txfBrugteVarer.clear();
			txfTilbageleveredeFustager.clear();
			cbAfleveret.setDisable(true);
		    btnAfregnPris.setDisable(true);
				
		if (lvwProdukter.hasProperties()) {
			lvwProdukter.getItems().clear();
		}
		//Opdaterer listviewet med alle de produkter, som er udlejede.
		LejetProdukt lp = null;		
		for (Pris p: controller.getPriser()) {
			if(p.getProdukt() instanceof LejetProdukt) 
			{
				lp = (LejetProdukt) p.getProdukt();
				if (lp.isErUdlejet()) {
					lvwProdukter.getItems().add(p);
				}
			}
			
		}

	}
	/*
	 * Metoden får fat i det valgte produkt i listviewet og foretager en udregning på
	 * baggrund panten, antallet af brugte varer og tilbageleverede fustager, hvoefter den endelige
	 * afregning vises i et TextArea
	 */
	private void afregnPris() 
	{
		if(!btnAfregnPris.isDisabled()) 
		{
			if(txfBrugteVarer.getText() != null && txfTilbageleveredeFustager != null) 
			{
				Pris p = lvwProdukter.getSelectionModel().getSelectedItem();
				if(p.getProdukt() instanceof LejetProdukt) 
				{
					String brugteVarer = txfBrugteVarer.getText();
					String tilbageleveredeFustager = txfTilbageleveredeFustager.getText();
					int brugteVarerInt = Integer.parseInt(brugteVarer);
					int tilbageleveredeFustagerInt = Integer.parseInt(tilbageleveredeFustager);					
					double afregnetPris = controller.getAfregningspris(p) -(brugteVarerInt - tilbageleveredeFustagerInt);				
					lvwProdukter.getItems().remove(p);
					infoBox.setText(txfUdlejer.getText() + " Har afleveret produktet: " + p.getProdukt().getNavn() + ". Den endelige pris er " + afregnetPris);
					
				}
				
			
			}
		}
	}
	
}

