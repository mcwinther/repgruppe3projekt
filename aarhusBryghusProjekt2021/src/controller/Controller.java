package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import model.Event;
import model.LejetProdukt;
import model.Ordre;
import model.Ordrelinje;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import storage.Storage;

public class Controller {

	private Storage storage;
	private static Controller controller;

	private Controller() {
		storage = new Storage();
	}

	/*
	 * singleton pattern controller objekt er uniqueInstance som kun kan tilgåes og
	 * oprettes gennem getController metode.
	 */

	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}

	// create metoder

	public Ordre createOrdre(int ordrerNummer, String navn, String beskrivelse, String betalingsform,
			LocalDate salgsDato) {
		Ordre ordre = new Ordre(ordrerNummer, navn, beskrivelse, betalingsform, salgsDato);
		storage.addOrdre(ordre);
		return ordre;
	}

	public Ordrelinje createOrdrelinje(int antal) {
		Ordrelinje ordrelinje = new Ordrelinje(antal);
		storage.addOrdreLinje(ordrelinje);
		return ordrelinje;
	}

	public Pris createPris(double beløb, double pant, int klippekort, Prisliste prisliste, Produkt produkt) {
		Pris pris = new Pris(beløb, pant, klippekort, prisliste, produkt);
		storage.addPris(pris);
		return pris;
	}

	public Prisliste createPrisliste(String prislisteType) {
		Prisliste prisliste = new Prisliste(prislisteType);
		storage.addPrisliste(prisliste);
		return prisliste;
	}

	public Produkt createProdukt(String navn, String beskrivelse) {
		Produkt produkt = new Produkt(navn, beskrivelse);
		storage.addProdukt(produkt);
		return produkt;

	}

	public Event createEvent(String navn, String beskrivelse, LocalDate dato, String arrangør) {
		Event event = new Event(navn, beskrivelse, dato, arrangør);
		storage.addEvent(event);
		return event;
	}

	public LejetProdukt createLejetProdukt(String navn, String beskrivelse, boolean erUdlejet) {
		LejetProdukt lejetProdukt = new LejetProdukt(navn, beskrivelse, erUdlejet);
		storage.addLejetProdukt(lejetProdukt);
		return lejetProdukt;
	}

	// get-metoder

	public List<Ordre> getOrdrer() {
		return storage.getOrdrer();
	}

	public List<Ordrelinje> getOrdrelinje() {
		return storage.getOrdrelinje();
	}

	public List<Pris> getPriser() {
		return storage.getPris();
	}

	public List<Produkt> getProdukter() {
		return storage.getProdukt();
	}

	public List<Prisliste> getPrislister() {
		return storage.getPrisliste();
	}

	public List<Event> getEvents() {
		return storage.getEvents();
	}

	public List<LejetProdukt> getLejedeProdukter() {
		return storage.getLejedeProdukter();
	}

	// Update metoder

	public void updateOrdre(Ordre ordre, int ordrerNummer, String navn, String beskrivelse, String betalingsform,
			boolean bogført, LocalDate salgsDato) {

		ordre.setOrdrerNummer(ordrerNummer);
		ordre.setNavn(navn);
		ordre.setBeskrivelse(beskrivelse);
		ordre.setBetalingsform(betalingsform);
		ordre.setSalgsDato(salgsDato);

	}

	public void updateOrdrelinje(Ordrelinje ordrel, int antal) {
		ordrel.setAntal(antal);
	}

	public void updateProdukt(Produkt produkt, String navn, String beskrivelse, boolean erUdlejet, boolean brugt,
			LocalDate dato, String arrangør, LocalTime tidspunkt) {

		produkt.setNavn(navn);
		produkt.setBeskrivelse(beskrivelse);

		if (produkt instanceof LejetProdukt) {

			((LejetProdukt) produkt).setErUdlejet(erUdlejet);
		} else if (produkt instanceof Event) {
			((Event) produkt).setArrangør(arrangør);
			((Event) produkt).setDato(dato);
		}
	}

	// Udlejning metoder

	public void udlejProdukt(LejetProdukt produkt) {
		if (produkt instanceof LejetProdukt) {
			checkUdlejning(produkt);
		}

	}

	private void checkUdlejning(LejetProdukt produkt) {
		if (produkt.isErUdlejet()) {
			produkt.setErUdlejet(false);
		} else {
			produkt.setErUdlejet(true);
		}
	}

	public double getAfregningspris(Pris pris) {
		double afregning = 0;
		if (pris.getProdukt() instanceof LejetProdukt) {
			afregning = pris.getBeløb() - pris.getPant();					
		}
		return afregning;
	}

	public void initStorage() {

		// Ordre

		Ordre o1 = controller.createOrdre(1, "Tom Hansen", "julegaver", "mastercard", LocalDate.of(2021, 4, 7));
		Ordre o2 = controller.createOrdre(2, "Marie Graversen", "privat", "mobilepay", LocalDate.of(2021, 4, 8));

		// ordrelinjer

		Ordrelinje ol1 = controller.createOrdrelinje(5);
		Ordrelinje ol2 = controller.createOrdrelinje(2);
		Ordrelinje ol3 = controller.createOrdrelinje(3);
		Ordrelinje ol4 = controller.createOrdrelinje(1);
		Ordrelinje ol5 = controller.createOrdrelinje(2);

		// Prisliste

		Prisliste pl1 = controller.createPrisliste("Fredagsbar");
		Prisliste pl2 = controller.createPrisliste("Butik");

		// Produkter

		Produkt f1 = controller.createProdukt("Klosterbryg", "flaske");
		Produkt f2 = controller.createProdukt("Sweet Georgia Brown", "flaske");
		Produkt f3 = controller.createProdukt("Extra Pilsner", "flaske");
		Produkt f4 = controller.createProdukt("Celebration", "flaske");
		Produkt f5 = controller.createProdukt("Blondie", "flaske");
		Produkt f6 = controller.createProdukt("Forårsbryg", "flaske");
		Produkt f7 = controller.createProdukt("India Pale Ale", "flaske");
		Produkt f8 = controller.createProdukt("Julebryg", "flaske");
		Produkt f9 = controller.createProdukt("Juletønden", "flaske");
		Produkt f10 = controller.createProdukt("Old Strong Ale", "flaske");
		Produkt f11 = controller.createProdukt("Fregatten Jylland", "flaske");
		Produkt f12 = controller.createProdukt("Imperial Stout", "flaske");
		Produkt f13 = controller.createProdukt("Tribute", "flaske");
		Produkt f14 = controller.createProdukt("Black monster", "flaske");

		Produkt fd1 = controller.createProdukt("Klosterbryg", "Fadøl, 40cl");
		Produkt fd2 = controller.createProdukt("Sweet Georgia Brown", "Fadøl, 40cl");
		Produkt fd3 = controller.createProdukt("Extra Pilsner", "Fadøl, 40cl");
		Produkt fd4 = controller.createProdukt("Celebration", "Fadøl, 40cl");
		Produkt fd5 = controller.createProdukt("Blondie", "Fadøl, 40cl");
		Produkt fd6 = controller.createProdukt("Forårsbryg", "Fadøl, 40cl");
		Produkt fd7 = controller.createProdukt("India Pale Ale", "Fadøl, 40cl");
		Produkt fd8 = controller.createProdukt("Julebryg", "Fadøl, 40cl");
		Produkt fd9 = controller.createProdukt("Juletønden", "Fadøl, 40cl");
		Produkt fd10 = controller.createProdukt("Old Strong Ale", "Fadøl, 40cl");
		Produkt fd11 = controller.createProdukt("Fregatten Jylland", "Fadøl, 40cl");
		Produkt fd12 = controller.createProdukt("Imperial Stout", "Fadøl, 40cl");
		Produkt fd13 = controller.createProdukt("Tribute", "Fadøl, 40cl");
		Produkt fd14 = controller.createProdukt("Black monster", "Fadøl, 40cl");

		Produkt s1 = controller.createProdukt("Whiskey 45%", "spiritus");
		Produkt s2 = controller.createProdukt("Whiskey 4cl", "spiritus");
		Produkt s3 = controller.createProdukt("Liquor of Aarhus", "spiritus");
		Produkt s4 = controller.createProdukt("Lyng gin 50cl", "spiritus");
		Produkt s5 = controller.createProdukt("Lyng gin 4cl", "spiritus");
		Produkt s6 = controller.createProdukt("Whiskey 4cl", "spiritus");

		Produkt e1 = controller.createEvent("Rundvisning", "privat", LocalDate.of(2021, 04, 07), "Aarhus Bryghus");

		Produkt g1 = controller.createProdukt("Glas", "Glas - uanset størrelse");

		Produkt fus1 = controller.createLejetProdukt("Klosterbryg 20L", "fustage", false);
		Produkt fus2 = controller.createLejetProdukt("Jazz classic 25L", "fustage", false);
		Produkt fus3 = controller.createLejetProdukt("Extra Pilsner 25L", "fustage", false);
		Produkt fus4 = controller.createLejetProdukt("Celebration 20L", "fustage", false);
		Produkt fus5 = controller.createLejetProdukt("Blondie 25L", "fustage", false);
		Produkt fus6 = controller.createLejetProdukt("Forårsbryg 20L", "fustage", false);
		Produkt fus7 = controller.createLejetProdukt("India Pale Ale 20L", "fustage", false);
		Produkt fus8 = controller.createLejetProdukt("Julebryg 20L", "fustage", false);
		Produkt fus9 = controller.createLejetProdukt("Imperial Stout 20L", "fustage", false);
		Produkt kulsyrePatroner = controller.createLejetProdukt("Kulsyre 6kg", "Kulsyrepatron", false);

		Produkt anlæg1 = controller.createLejetProdukt("1-hane", "Anlæg", false);
		Produkt anlæg2 = controller.createLejetProdukt("2-hane", "Anlæg", false);
		Produkt anlæg3 = controller.createLejetProdukt("Bar med flere haner", "Anlæg", false);

		// Pris

		Pris p1 = controller.createPris(70, 0, 2, pl1, f1);
		Pris p1_2 = controller.createPris(36, 0, 0, pl2, f1);
		Pris p2 = controller.createPris(70, 0, 2, pl1, f2);
		Pris p2_2 = controller.createPris(36, 0, 0, pl2, f2);
		Pris p3 = controller.createPris(70, 0, 2, pl1, f3);
		Pris p3_2 = controller.createPris(36, 0, 0, pl2, f3);
		Pris p4 = controller.createPris(70, 0, 2, pl1, f4);
		Pris p4_2 = controller.createPris(36, 0, 0, pl2, f4);
		Pris p5 = controller.createPris(70, 0, 2, pl1, f5);
		Pris p5_2 = controller.createPris(36, 0, 0, pl2, f5);
		Pris p6 = controller.createPris(70, 0, 2, pl1, f6);
		Pris p6_2 = controller.createPris(36, 0, 0, pl2, f6);

		Pris p7 = controller.createPris(599, 0, 0, pl1, s1);
		Pris p7_2 = controller.createPris(599, 0, 0, pl2, s1);
		Pris p8 = controller.createPris(50, 0, 0, pl1, s2);
		Pris p9 = controller.createPris(175, 0, 0, pl1, s3);
		Pris p9_2 = controller.createPris(175, 0, 0, pl2, s3);
		Pris p10 = controller.createPris(350, 0, 0, pl1, s3);
		Pris p10_2 = controller.createPris(350, 0, 0, pl2, s3);
		Pris p11 = controller.createPris(40, 0, 0, pl1, s4);

		Pris p12 = controller.createPris(15, 0, 0, pl2, g1);
		Pris p13 = controller.createPris(100, 0, 0, pl2, e1);

		Pris p14 = controller.createPris(70, 0, 0, pl1, f1);
		Pris p15 = controller.createPris(36, 0, 0, pl2, f1);

		Pris p20 = controller.createPris(775, 200, 0, pl2, fus1);
		Pris p21 = controller.createPris(625, 200, 0, pl2, fus2);
		Pris p22 = controller.createPris(575, 200, 0, pl2, fus3);
		Pris p23 = controller.createPris(775, 200, 0, pl2, fus4);
		Pris p24 = controller.createPris(700, 200, 0, pl2, fus5);

		Pris p25 = controller.createPris(38, 0, 1, pl1, fd1);
		Pris p26 = controller.createPris(38, 0, 1, pl1, fd2);
		Pris p27 = controller.createPris(38, 0, 1, pl1, fd3);
		Pris p28 = controller.createPris(38, 0, 1, pl1, fd4);
		Pris p29 = controller.createPris(38, 0, 1, pl1, fd5);
		Pris p30 = controller.createPris(38, 0, 1, pl1, fd6);
		Pris p31 = controller.createPris(38, 0, 1, pl1, fd7);
		Pris p32 = controller.createPris(38, 0, 1, pl1, fd8);
		Pris p33 = controller.createPris(38, 0, 1, pl1, fd9);
		Pris p34 = controller.createPris(38, 0, 1, pl1, fd10);
		Pris p35 = controller.createPris(38, 0, 1, pl1, fd11);
		Pris p36 = controller.createPris(38, 0, 1, pl1, fd12);
		Pris p37 = controller.createPris(38, 0, 1, pl1, fd13);
		Pris p38 = controller.createPris(38, 0, 1, pl1, fd14);

		Pris p39 = controller.createPris(775, 200, 0, pl2, fus6);
		Pris p40 = controller.createPris(775, 200, 0, pl2, fus7);
		Pris p41 = controller.createPris(775, 200, 0, pl2, fus8);
		Pris p42 = controller.createPris(775, 200, 0, pl2, fus9);

		Pris p43 = controller.createPris(250, 200, 0, pl2, fus7);
		Pris p44 = controller.createPris(400, 200, 0, pl2, fus8);
		Pris p45 = controller.createPris(500, 200, 0, pl2, fus9);
		Pris p46 = controller.createPris(400, 1000, 0, pl2, kulsyrePatroner);

		// Ordrelinjes antal sættes til et pris-objekt

		ol1.setPris(p1);
		ol2.setPris(p11);
		ol3.setPris(p9);
		ol4.setPris(p20);
		ol5.setPris(p24);

		// Forbindelser skabes her mellem ordre og ordrelinje

		o1.addOrdrelinje(ol1);
		o1.addOrdrelinje(ol3);
		o2.addOrdrelinje(ol2);
		o2.addOrdrelinje(ol4);
		o1.addOrdrelinje(ol5);

	}

	/*
	 * Objekterne som er oprettet i Init-Storage initialiseres med metoden init()
	 * som kaldes i GUI-laget
	 */

	public void init() {
		initStorage();

	}

}
