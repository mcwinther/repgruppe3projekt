package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import model.Ordre;
import model.Ordrelinje;
import model.Pris;
import model.Prisliste;
import model.Produkt;

public class PrisListeTest {

	Ordre minOrdre;
	Ordrelinje ol;
	Ordrelinje ol2;
	Ordrelinje ol3;
	Ordrelinje ol4;
	Ordrelinje ol5;
	Ordrelinje ol6;
	Pris p1;
	Pris p2;
	Pris p3;
	Pris p4;
	Pris p5;
	Pris p6;
	Produkt pr1;
	Produkt pr2;
	Produkt pr3;
	Produkt pr4;
	Produkt pr5;
	Prisliste pl1;

	@Before
	public void setUp() throws Exception {
		minOrdre = new Ordre(1, "Tom Hansen", "julegaver", "mastercard", LocalDate.of(2021, 4, 7));
		ol = new Ordrelinje(3);
		ol2 = new Ordrelinje(2);
		ol3 = new Ordrelinje(1);
		ol4 = new Ordrelinje(4);
		ol5 = new Ordrelinje(1);
		ol6 = new Ordrelinje(2);
		pl1 = new Prisliste("Fredagsbar");
		pr1 = new Produkt("Klosterbryg", pl1.getPrisListeType());
		pr2 = new Produkt("Black Monster", pl1.getPrisListeType());
		pr3 = new Produkt("t-shirt", pl1.getPrisListeType());
		pr4 = new Produkt("Imperial Stout", pl1.getPrisListeType());
		pr5 = new Produkt("Lyng gin", pl1.getPrisListeType());
		p1 = new Pris(70, 10, 0, pl1, pr1);
		p2 = new Pris(100, 10, 0, pl1, pr2);
		p3 = new Pris(70, 10, 0, pl1, pr3);
		p4 = new Pris(70, 10, 0, pl1, pr4);
		p5 = new Pris(350, 10, 0, pl1, pr5);
		p6 = new Pris(2, 10, 0, pl1, pr5);

	}

	@Test
	public void testBeregnSamletPris() {
		minOrdre.addOrdrelinje(ol);
		minOrdre.addOrdrelinje(ol2);
		ol.setPris(p1);
		ol2.setPris(p2);
		double result = minOrdre.beregnSamletPris();
		System.out.println(result);
		assertEquals(410, result, 0.001);
	}

	@Test
	// For testcase 2
	public void testBeregnSamletPris2() {
		minOrdre.addOrdrelinje(ol3);
		minOrdre.addOrdrelinje(ol4);
		minOrdre.addOrdrelinje(ol2);
		minOrdre.addOrdrelinje(ol6);
		minOrdre.addOrdrelinje(ol5);
		ol3.setPris(p1);
		ol4.setPris(p2);
		ol2.setPris(p3);
		ol6.setPris(p4);
		ol5.setPris(p5);
		double result = minOrdre.beregnSamletPris();
		assertEquals(1100, result, 0.001);
	}

	@Test

	public void testPrisliste() {
		assertEquals("Fredagsbar", pl1.getPrisListeType());
		

	}

	@Test

	public void testPrisListeError() {
		pl1.setPrisListeType(null);
		assertFalse(pl1.getPrisListeType() != null);
	}

	@Test

	public void testGetPris() {
		assertTrue(pl1.getPris() != null);

	}

	@Test

	public void testGetPrisError() {
		pl1.removePris(p1);
		pl1.removePris(p2);
		pl1.removePris(p3);
		pl1.removePris(p4);
		pl1.removePris(p5);
		pl1.removePris(p6);

		assertTrue(pl1.getPris().size() == 0);

	}

	@Test
	public void testAddPris() {
		pl1.addPris(p1);
		assertTrue(pl1.getPris().contains(p1));

	}

	@Test
	public void testAddPrisError() {
		pl1.removePris(p1);
		pl1.removePris(p2);
		pl1.removePris(p3);
		pl1.removePris(p4);
		pl1.removePris(p5);
		pl1.removePris(p6);

		pl1.addPris(p1);
		assertEquals(1, pl1.getPris().size());
		pl1.addPris(p1);
		assertEquals(1, pl1.getPris().size());

		try {
			Pris pris = pl1.createPris(-50, 10, 0, pl1, pr1);
			pl1.addPris(pris);
			
		} catch (ArithmeticException e) {
			System.out.println("Beløb er mindre end 0");
		}
	}

	@Test
	public void testRemovePris() {
		pl1.removePris(p1);
		assertFalse(pl1.getPris().contains(p1));
	}

	@Test
	public void testRemovePrisError() {
		pl1.removePris(p1);
		assertEquals(5, pl1.getPris().size());
		pl1.removePris(p1);
		assertEquals(5, pl1.getPris().size());
		
		try {
			Pris pris = pl1.createPris(-50, 10, 0, pl1, pr1);
			pl1.addPris(pris);
			pl1.removePris(pris);
			
		} catch (ArithmeticException e) {
			System.out.println("Beløb er mindre end 0");
		}
	}

	@Test
	public void testCreatePris() {
		Pris pris1 = pl1.createPris(100, 10, 0, pl1, pr1);
		assertNotNull(pris1);

		pris1.setProdukt(null);
		assertTrue(pris1.getProdukt() == null);

		pris1.setPrisListe(null);
		assertTrue(pris1.getPrisListe() == null);
	}

	@Test
	public void testCreatePrisError() {
		try {
			Pris pris2 = pl1.createPris(-50, 10, 0, pl1, pr1);
			
		} catch (ArithmeticException e) {
			System.out.println("Beløb er mindre end 0");
		}
		
		/*
		 * Da hele objektet ikke kunne oprettes kan man ikke 'set' prisliste eller
		 * produkt til at være null, som vi ellers havde gjort i testCreatePris.
		 */

	}

	@Test
	public void testGetPrisListeType() {
		assertNotNull(pl1.getPrisListeType());
	}

	@Test
	public void testGetPrisListeTypeError() {
		pl1.setPrisListeType(null);
		assertNull(pl1.getPrisListeType());
	}

	@Test
	public void testSetPrisListeType() {
		pl1.setPrisListeType("Hej");
		assertEquals("Hej", pl1.getPrisListeType());
	}

	@Test
	public void testSetPrisListeTypeError() {
		pl1.setPrisListeType("");
		assertFalse(pl1.getPrisListeType() == "Hej");
	}

}
