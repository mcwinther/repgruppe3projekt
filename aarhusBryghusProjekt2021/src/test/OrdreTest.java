package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import model.Ordre;
import model.Ordrelinje;

public class OrdreTest {

	Ordre o1;
	Ordrelinje ol1;
	Ordrelinje ol2;

	@Before
	public void setUp() throws Exception {

		ol1 = new Ordrelinje(2);
		o1 = new Ordre(1, "Tom Hansen", "julegaver", "mastercard", LocalDate.of(2021, 4, 7));
		o1.addOrdrelinje(ol1);
	}

	@Test
	public void GetSamletAntalTC1() {
		Ordrelinje ol2 = new Ordrelinje(-4);
		o1.addOrdrelinje(ol2);
		assertEquals(-2, o1.getSamletAntal());
	}

	@Test
	public void GetSamletAntalTC2() {
		Ordrelinje ol2 = new Ordrelinje(-2);
		o1.addOrdrelinje(ol2);
		assertEquals(0, o1.getSamletAntal());
	}

	@Test
	public void GetSamletAntalTC3() {
		Ordrelinje ol2 = new Ordrelinje(3);
		o1.addOrdrelinje(ol2);
		assertEquals(5, o1.getSamletAntal());
	}

}
