package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.LejetProdukt;

public class ControllerTest {

	LejetProdukt l1;

	@Before
	public void setUp() throws Exception {

		l1 = new LejetProdukt("Klosterbryg 20L", "fustage", true);

	}

	@Test
	public void CheckUdlejningTC1() {
		assertEquals(true, l1.isErUdlejet());
	}

	@Test
	public void CheckUdlejningTC2() {
		assertEquals(false, !l1.isErUdlejet());
	}

}
