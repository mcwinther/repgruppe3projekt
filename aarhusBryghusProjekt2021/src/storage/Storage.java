package storage;

import java.util.ArrayList;
import java.util.List;

import model.Event;
import model.LejetProdukt;
import model.Ordre;
import model.Ordrelinje;
import model.Pris;
import model.Prisliste;
import model.Produkt;

public class Storage {
	private List<Prisliste> prisliste;
	private List<Produkt> produkt;
	private List<Ordrelinje> ordrelinje;
	private List<Ordre> ordrer;
	private List<Pris> pris;
	private List<LejetProdukt> lejedeprodukter;
	private List<Event> event;
	
	public Storage() {
		prisliste = new ArrayList<Prisliste>();
		produkt = new ArrayList<Produkt>();
		ordrelinje = new ArrayList<Ordrelinje>();
		ordrer = new ArrayList<Ordre>();
		pris = new ArrayList<Pris>();
		lejedeprodukter = new ArrayList<LejetProdukt>();
		event = new ArrayList<Event>();

	}
	
	public List<Prisliste> getPrisliste() {
		return new ArrayList<Prisliste> (prisliste);
	}
	

	public void addPrisliste(Prisliste prislister) {
		if (!prisliste.contains(prislister)) {
			prisliste.add(prislister);
		}
	}

	public List<Produkt> getProdukt() {
		return new ArrayList<Produkt>(produkt);
	}


	public void addProdukt(Produkt produkter) {
		if (!produkt.contains(produkter)) {
			produkt.add(produkter);
		}
	}

	public List<Ordrelinje> getOrdrelinje() {
		return new ArrayList<Ordrelinje> (ordrelinje);
	}


	public void addOrdreLinje(Ordrelinje ordrelinjer) {
		if (!ordrelinje.contains(ordrelinjer)) {
			ordrelinje.add(ordrelinjer);
		}
	}


	public List<Ordre> getOrdrer() {
		return new ArrayList<Ordre> (ordrer);
	}

	public void addOrdre(Ordre ordrerne) {
		if (!ordrer.contains(ordrerne)) {
			ordrer.add(ordrerne);
		}
	}
	public List<Pris> getPris() {
		return new ArrayList<Pris> (pris);
	}
	
	public void addPris(Pris priser) {
		if (!pris.contains(priser)) {
			pris.add(priser);
		}
	}
	
	public List<LejetProdukt> getLejedeProdukter() {
		return new ArrayList<LejetProdukt> (lejedeprodukter);
	}
	
	public void addLejetProdukt(LejetProdukt lejetprodukt) {
		if (!lejedeprodukter.contains(lejetprodukt)) {
			lejedeprodukter.add(lejetprodukt);
		}
	}
	
	public List<Event> getEvents() {
		return new ArrayList<Event> (event);
	}
	
	public void addEvent(Event eventer) {
		if (!event.contains(eventer)) {
			event.add(eventer);
		}
	}
	
	
	
}
